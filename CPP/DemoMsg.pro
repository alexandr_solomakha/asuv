#-------------------------------------------------
#
# Project created by QtCreator 2015-10-21T16:50:37
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = DemoMsg
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -L/usr/local/lib/ -lprotobuf

SOURCES += main.cpp \
    messages.pb.cc

HEADERS += messages.pb.h

DISTFILES += \
    ../messages.proto
