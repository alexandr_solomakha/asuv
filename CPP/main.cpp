#include <QCoreApplication>
#include "messages.pb.h"
#include <QDateTime>
#include <QUuid>
#define MAX_BUFFER 1024

GUID createNewGuid()
{
	GUID value;
	QByteArray array = QUuid::createUuid().toRfc4122();
	quint64 l = *(quint64*)array.data();
	quint64 h = *((quint64*)array.data()+1);
	value.set_hp(h);
	value.set_lp(l);
	return value;
}

int main(int argc, char *argv[])
{
	GUID myId = createNewGuid();
	GUID DestinationId = createNewGuid();
	char buffer[MAX_BUFFER];
	memset(buffer, 0, MAX_BUFFER);
	QCoreApplication a(argc, argv);

	//create text message
	BASEMESSAGE msg;
	*msg.mutable_fromcallsigns() = myId;
	*msg.mutable_tocallsigns()  = DestinationId;
	msg.set_creationtime(QDateTime::currentDateTime().toMSecsSinceEpoch());
	msg.set_type(BASEMESSAGE::TEXT);

	//create plain text message
	TEXTMESSAGE textmsg;
	textmsg.set_message("test message");
	textmsg.SerializeToArray(buffer, MAX_BUFFER);
	int szTxt = textmsg.ByteSize();
	//in the buffer we have serialized message with the size szTxt
	//there we can encryp buffer with the AES256 or something else

	//put encrypted data into transport message
	msg.set_body(buffer, szTxt);
	//erase buffer
	memset(buffer, 0, MAX_BUFFER);
	//serialize transport message wint the text message into raw data
	msg.SerializeToArray(buffer, MAX_BUFFER);
	int sz = msg.ByteSize();
	//in the buffer we have srialized message
	//in the sz we have size of the raw bytes

	//================================================
    //so doing deserialization plain text
	BASEMESSAGE msgEncoded;
	msgEncoded.ParseFromArray(buffer, sz);

	//check value of the original and deserialized objects
	assert(msgEncoded.type() == msg.type());
	//assert(*msgEncoded.mutable_tocallsigns() == *msg.mutable_tocallsigns());
	//assert(msgEncoded.fromcallsigns() == msg.fromcallsigns());
	assert(msgEncoded.creationtime() == msg.creationtime());

    TEXTMESSAGE textmsgDeserialized;
    textmsgDeserialized.ParseFromString(msgEncoded.body());
    //check valuse
    assert(textmsgDeserialized.message() == textmsg.message());

    //Examples
    //==============================================================================
    //==============================================================================
    //Tank in the position 48.0 38.0
    BASEMESSAGE msgTank;
	*msgTank.mutable_fromcallsigns() = myId;
	*msgTank.mutable_tocallsigns()  = DestinationId;
	msgTank.set_creationtime(QDateTime::currentDateTime().toMSecsSinceEpoch());
    msgTank.set_type(BASEMESSAGE::INTREP);

	INTREPMESSAGE intrep1;

    OBJECTITEM* value = intrep1.add_objectitems();
    value->mutable_id()->CopyFrom(createNewGuid());

    //set type of the object
    value->set_type(T062_01); //TANK
    value->set_host(HO); //Hostile	An OBJECT-ITEM that is positively identified as enemy.

    //set coordinates
    value->mutable_location()->set_latitude(48.0);
	value->mutable_location()->set_longitude(38.0);
    intrep1.SerializeToString(msgTank.mutable_body());

    //erase buffer and serialize
    memset(buffer, 0, MAX_BUFFER);
    msgTank.SerializeToArray(buffer, MAX_BUFFER);
    int szMsgTank = msgTank.ByteSize();

    //==============================================================================
    //==============================================================================
    //Contr attack 10 BTR at the position 48.0 38.0
    BASEMESSAGE msgCommand;
	*msgCommand.mutable_fromcallsigns() = myId;
	*msgCommand.mutable_tocallsigns()  = DestinationId;
	msgCommand.set_creationtime(QDateTime::currentDateTime().toMSecsSinceEpoch());
    msgCommand.set_type(BASEMESSAGE::INTREP);

	INTREPMESSAGE intrep2;

    value = intrep2.add_objectitems();
    value->mutable_id()->CopyFrom(createNewGuid());

    //set type of the object
    value->set_type(T062_05); //BRDM
    value->set_host(HO); //Hostile	An OBJECT-ITEM that is positively identified as enemy.

    //set coordinates
    value->mutable_location()->set_latitude(48.0);
	value->mutable_location()->set_longitude(38.0);
	value->set_count(10);
	value->mutable_action()->set_action("Attack this object.");

    intrep2.SerializeToString(msgCommand.mutable_body());

    //erase buffer and serialize
    memset(buffer, 0, MAX_BUFFER);
    msgCommand.SerializeToArray(buffer, MAX_BUFFER);
    int szMsgCommand = msgCommand.ByteSize();

	return a.exec();
}



